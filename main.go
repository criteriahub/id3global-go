package main

import (
	"crypto/tls"
	"log"
	"time"

	"github.com/hooklift/gowsdl/soap"

	"bitbucket.org/criteriahub/id3global-go/id3global"
)

func main() {
	client := soap.NewClient(
		"https://pilot.id3global.com/ID3gWS/ID3global.svc?wsdl",
		soap.WithTimeout(time.Second*5),
		soap.WithBasicAuth("", ""),
		soap.WithTLS(&tls.Config{InsecureSkipVerify: true}),
	)
	var ref string
	service := id3global.NewIGlobalAuthenticate(client)
	reply, err := service.AuthenticateSP(&id3global.AuthenticateSP{
		CustomerReference: &ref,
	})
	if err != nil {
		log.Fatalf("Expect a 404 as there is no customer ref: %v", err)
	}
	log.Println(reply)
}
